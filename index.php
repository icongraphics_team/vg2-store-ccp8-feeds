<?php
    global $argc, $argv;
    error_reporting(-1);
    ini_set('display_errors', 'On');

    require 'functions.php';
    require 'Classes/Maintenance.php';
    require 'Classes/Validation.php';
    require 'Classes/Action.php';
    require 'Classes/Feed.php';
    require 'Classes/DBH.php';
    require 'Classes/Product.php';
    require 'Classes/Products.php';
    require 'Classes/Category.php';
    require 'Classes/Categories.php';
    require 'Classes/Filter.php';
    require 'Classes/Filters.php';
    require 'Classes/Log.php';
    //Lipseys
    require 'Classes/sources/Lipseys/LipseysCategory.php';
    require 'Classes/sources/Lipseys/LipseysCategories.php';
    require 'Classes/sources/Lipseys/LipseysProducts.php';
    require 'Classes/sources/Lipseys/LipseysProduct.php';
    require 'Classes/sources/Lipseys/LipseysFeed.php';
    require 'Classes/sources/Lipseys/LipseysFilter.php';
    require 'Classes/sources/Lipseys/LipseysFilters.php';
    
    define( 'XMLOUTPUTPATH', __DIR__ . '/storage/feeds' );
    define( 'PRODUCTIMAGEPATH', __DIR__ . '/storage/images' );
    define( 'FINALPRODUCTIMAGEPATHSM', __DIR__ . '/../../public_html/media/ecom/prodsm' );
    define( 'FINALPRODUCTIMAGEPATHLG', __DIR__ . '/../../public_html/media/ecom/prodlg' );
    define( 'FINALPRODUCTIMAGEPATHXL', __DIR__ . '/../../public_html/media/ecom/prodxl' );

    define( 'PRODFILTERONE', 'caliber' );
    define( 'PRODFILTERTWO', 'action' );
    define( 'PRODFILTERTHREE', 'type' );
    define( 'PRODFILTERFOUR', 'model' );
    define( 'PRODFILTERFIVE', 'mfg' );

    define( 'LOGPATH', __DIR__ . '/logs' );

    // http://randomkeygen.com/
    $keys = array(
        "maintenance" => "8CzSmM",
        "ecom-inv-load" => "Dk2IYh",
        "ecom-prod-load" => "Z744F5",
        "ecom-image-resize" => "G728SQ",
        "ecom-cat-load" => "R9S4S8",
        "ecom-filter-load" => "F8TY32",
        "phpinfo" => "phpinfo"
    );
    $actions = array(
        "maintenance" => "maintenance",
        "ecom-prod-load" => "ecomProdLoad",
        "ecom-inv-load" => "ecomInvLoad",
        "ecom-image-resize" => "ecomImageResize",
        "ecom-cat-load" => "ecomCatLoad",
        "ecom-filter-load" => "ecomFilterLoad",
        "phpinfo" => "phpinfo"
    );

    $validation = new VG\Validation( $keys, $actions );
    if( PHP_SAPI === "cli" ){
        $validation->submitted_key = $argv[1];
        $validation->submitted_action = $argv[2];
        $validation->submitted_xoption = ( isset( $argv[3] ) ) ? $argv[3] : false;
    } else{
        $validation->submitted_key = $_GET['key'];
        $validation->submitted_action = $_GET['action'];
        $validation->submitted_xoption = ( isset( $_GET['xoption'] ) ) ? $_GET['xoption'] : false;
    }
    
    if( $validation->verify() === true ){
        $action = new VG\Action( $validation->submitted_xoption );
        call_user_func( [ $action, $validation->actions[$validation->submitted_action] ] );
    } else{
        die("Invalid key/action! Provided: " . $argv[1] . $_GET['key'] . ' | ' . $argv[2] . $_GET['action']);
    }
?>