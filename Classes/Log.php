<?php

namespace VG;
use DateTime;
use DateTimeZone;

class Log{
    protected $log_path;
    protected $entry_data = "";

    public function __construct(){
        $this->log_path = LOGPATH . '/LOG_' . $this->getDateTime( 'date_compressed') . '.txt';
    }

    /**
     * getDateTime
     * Gets current date and returns them in requested format.
     * @param string $format                Desired date format.
     * @return string                       Formatted date.
     */
    protected function getDateTime( $format = "full" ){
        $date_time_raw = new DateTime;
        $time_zone = new DateTimeZone( 'America/New_York' );
        $date_time_raw->setTimezone( $time_zone );

        switch ( $format ){
            case "full":
                return $date_time_raw->format( 'M d, Y @ G:i' );
                break;
            case "date_only":
                return $date_time_raw->format( 'M d, Y' );
                break;
            case "time_only":
                return $date_time_raw->format( 'G:i' );
                break;
            case "date_compressed":
                return $date_time_raw->format( 'MdY' );
                break;
        }
    }

    /**
     * createEntry
     * Creates a new line in the log
     * @param string $log_data                  Data to add to the new line in the log.
     */
    public function createEntry( $log_data = "Empty Log Entry \n" ){
        $this->entry_data .= "{$this->getDateTime( 'time_only' )} {$log_data}\n";
    }

    /**
     * writeEntry
     * Takes all entries created and writes them to the log file.
     */
    public function writeEntry(){
        if( file_exists( $this->log_path ) ){
            $current_log_file = file_get_contents( $this->log_path );
        } else{
            $current_log_file = "###LOG FILE FOR {$this->getDateTime( 'date_only' )}###\n";
        }

        file_put_contents( $this->log_path, $current_log_file . $this->entry_data );
    }

}