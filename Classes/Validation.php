<?php

namespace VG;

class Validation{

    public $submitted_key;
    public $submitted_action;
    public $submitted_xoption;
    protected $keys;
    public $actions;

    public function __construct( array $keys, array $actions){
        $this->keys = $keys;
        $this->actions = $actions;
    }

    public function verify(){
        if( array_key_exists( $this->submitted_action, $this->keys ) && $this->keys[$this->submitted_action] === $this->submitted_key) {
            return true;
        } else{
            return false;
        }
    }
}

?>