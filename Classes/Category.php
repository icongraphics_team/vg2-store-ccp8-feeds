<?php
namespace VG;

Abstract class Category{
    public $id;
    public $name;
    public $description;
    public $catimg;
    public $xcat;
    public $type;

    public abstract function addToEcom();
}

?>