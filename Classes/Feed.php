<?php
namespace VG;

Abstract class Feed{
    protected $feed_provider;
    protected $feed_type;
    public $feed_url;
    public $destination_path;
    public $feed_items = array();

    public function __construct( $feed_type ){
        $this->feed_type = $feed_type;
    }

    public abstract function downloadFeed();
    public abstract function extractItems();
}

?>