<?php
namespace VG;

use \DateTime;
use \PDO;

class Maintenance{

    public function clearLogs(){
        
        $date = new DateTime;
        $date = $date->modify('-1 day');
        $yesterday = $date->format('%Y-%m-%d ') . '%';

        $dbh = DBH::getInstance("log");
        $sql = "DELETE FROM logs WHERE date_added LIKE :yesterday";
        $stmt = $dbh->prepare( $sql );
        $stmt->bindParam(':yesterday', $yesterday, PDO::PARAM_STR);
        $stmt->execute();
    }
}

?>