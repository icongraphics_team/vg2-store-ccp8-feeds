<?php

namespace VG;

class LipseysFeed extends Feed{
    protected $feed_provider = "Lipseys";
    protected $item_type;

    public function __construct( $feed_type, $item_type = false ){
        $this->feed_type = $feed_type;
        $this->item_type = $item_type;
    }

    /**
     * Downloads feed XML file to designated location.
     * @return string Path to XML file just downloaded.
     */
    public function downloadFeed(){
        //Check for feed type and fetch feed URL accordingly.
        if( $this->feed_type === "catalog" ) {
            $this->feed_url = "http://www.lipseys.com/API/catalog.ashx?email=sales@valleyguns2.com&pass=model70";
        } elseif( $this->feed_type === "inventory" ){
            $this->feed_url = "http://www.lipseys.com/API/pricequantitycatalog.ashx?email=sales@valleyguns2.com&pass=model70";
        }
        //Assemble destination path for downloading feed "/feed_type-currenttime.xml"
        $this->destination_path = XMLOUTPUTPATH . "/{$this->feed_provider}_{$this->feed_type}_{$this->item_type}.xml";
        //Add argument for item type to fetch
        if( $this->item_type !== false ) {
            $this->feed_url .= "&itemtype={$this->item_type}";
        }

        file_put_contents( $this->destination_path, file_get_contents( $this->feed_url ) );
        log_it( 'feed', 'Downloaded');

        return $this->destination_path;
    }

    /**
     * Extract items from the XML file
     * @return array An array of items extracted from the XML file
     */
    public function extractItems(){
        $feed = simplexml_load_file( $this->destination_path );
        foreach( $feed->Item as $item ) {
            $this->feed_items[trim( $item->UPC )] = get_object_vars( $item );
        }

        return $this->feed_items;
    }
}


?>