<?php
namespace VG;
use \PDO;

class LipseysCategory extends Category{
    public $splashdisp = 0;
    protected $main_cats = array(
        'Accessories',
        'Ammunition',
        'Firearm',
	    'AMMUNITION',
	    'Shotguns',
	    'Handguns',
	    'Rifles'
    );

    /**
     * LipseysCategory constructor.
     * Sets a few default parameters
     *
     * @param $type                 Type of product (category)
     */
    public function __construct( $type )
    {
//        foreach ( $category_info as $key => $value ){
//            $this->$key = $value;
//        }
        if ($type === "" || $type === "array" || $type === " ") {
            $type = "NO CATEGORY";
        }

        $this->name = $this->getCategoryName( $type );
        $this->id = make_id( $type );
        $this->description = "The {$this->name} category.";
        $this->splashdisp = ($this->isMainCat() === true) ? 1 : 0;

    }

    /**
     * getCategoryName
     * Takes $type parameter, and strips away any Lipseys-specified prefixes
     * @param $type                 Type of product
     * @return string               Returns category name with Lipseys prefixes removed.
     */
    protected function getCategoryName( $type ){
        $find = array(
            'Accessory-',
            'Accessory - ',
	        'ACCESSORY-',
	        'ACCESSORY - ',
            'NFA-',
            'NFA - ',
	        'NFA---',
	        'NFA ---',
	        '--',
        );

        $replace = ""; //Make array if replacing with anything but empty strings

        return str_replace( $find, $replace, $type );
    }

    /**
     * Checks if category is a main category.
     * @return bool                 true/false
     */
    protected function isMainCat(){
        return in_array( $this->name, $this->main_cats );
    }

    /**
     * Adds category to "ecom_cat" table in database.
     */
    public function addToEcom(){
        $dbh = DBH::getInstance("ecom");

        $sql = "INSERT INTO ecom_cat (id, name, keywords, description, catimg, splashdisp, subdispnum, subdisptype, proddispnum, proddisptype, catmulti, xcat, sortorder, catview, catviewg, datetimestamp, metatitle, metadesc, seourl, nofilter, autoadded) VALUES(:id, :name, :keywords, :description, :catimg, :splashdisp, :subdispnum, :subdisptype, :proddispnum, :proddisptype, :catmulti, :xcat, :sortorder, :catview, :catviewg, :datetimestamp, :metatitle, :metadesc, :seourl, :nofilter, :autoadded) ON DUPLICATE KEY UPDATE name=:name , keywords=:keywords , description=:description , subdispnum=:subdispnum , subdisptype=:subdisptype , proddispnum=:proddispnum , proddisptype=:proddisptype , catmulti=:catmulti , xcat=:xcat , sortorder=:sortorder , catview=:catview , catviewg=:catviewg , datetimestamp=:datetimestamp , metatitle=:metatitle , metadesc=:metadesc , seourl=:seourl , nofilter=:nofilter";
        $stmt = $dbh->prepare( $sql );
        $stmt->bindValue(':id', make_id( $this->id ), PDO::PARAM_STR );
        $stmt->bindValue(':name', $this->name, PDO::PARAM_STR );
        $stmt->bindValue(':keywords', $this->name, PDO::PARAM_STR );
        $stmt->bindValue(':description', $this->description, PDO::PARAM_STR );
        $stmt->bindValue(':catimg', '', PDO::PARAM_STR );
        $stmt->bindValue(':splashdisp', $this->splashdisp, PDO::PARAM_INT );
        $stmt->bindValue(':subdispnum', 1, PDO::PARAM_INT );
        $stmt->bindValue(':subdisptype', 'default', PDO::PARAM_STR );
        $stmt->bindValue(':proddispnum', 1, PDO::PARAM_INT );
        $stmt->bindValue(':proddisptype', 'VG2PRODDISPLAY', PDO::PARAM_STR );
        $stmt->bindValue(':catmulti', 0, PDO::PARAM_INT );
        $stmt->bindValue(':xcat', '', PDO::PARAM_STR );
        $stmt->bindValue(':sortorder', 100, PDO::PARAM_INT );
        $stmt->bindValue(':catview', 'A', PDO::PARAM_STR );
        $stmt->bindValue(':catviewg', '', PDO::PARAM_STR );
        $stmt->bindValue(':datetimestamp', time(), PDO::PARAM_STR );
        $stmt->bindValue(':metatitle', $this->description, PDO::PARAM_STR );
        $stmt->bindValue(':metadesc', $this->description, PDO::PARAM_STR );
        $stmt->bindValue(':seourl', $this->id, PDO::PARAM_STR );
        $stmt->bindValue(':nofilter', 0, PDO::PARAM_INT );
        $stmt->bindValue(':autoadded', 1, PDO::PARAM_INT );

        $stmt->execute();
    }

}

?>