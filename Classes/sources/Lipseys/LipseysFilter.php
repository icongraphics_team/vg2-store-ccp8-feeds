<?php
namespace VG;
use PDO;

class LipseysFilter{

    public function __construct( $name = null, $filter_type = null){


        if( !is_null( $name ) && !is_null( $filter_type ) ){
	        $alpha = range('A', 'Z');
	        $alpha[] = range(0,9);
	        $first_letter = $name[0];
	        $location = array_search($first_letter, $alpha);

            $name = clean_filter_name( $name );
            $this->id = make_id( $name );
            $this->filterslot = $this->getSlot( $filter_type );
            $this->filtername = $name;
            $this->sortorder = ($location !== false) ? $location : 101;
        }
    }

    protected function getSlot( $filter_type ){

        switch ( $filter_type ){
            case PRODFILTERONE:
                return 1;
                break;
            case PRODFILTERTWO:
                return 2;
                break;
            case PRODFILTERTHREE:
                return 3;
                break;
            case PRODFILTERFOUR:
                return 4;
                break;
            case PRODFILTERFIVE:
                return 5;
                break;
        }

    }

    public function addToEcom(){
        $dbh = DBH::getInstance("ecom");
        $sql = "INSERT IGNORE INTO ecom_prodfilter ( id, filterslot, filtername, sortorder ) VALUES( :id, :filterslot, :filtername, :sortorder )";
        $stmt = $dbh->prepare( $sql );
        $stmt->bindParam( ':id', $this->id, PDO::PARAM_STR );
        $stmt->bindParam( ':filterslot', $this->filterslot, PDO::PARAM_STR );
        $stmt->bindParam( ':filtername', $this->filtername, PDO::PARAM_STR );
        $stmt->bindParam( ':sortorder', $this->sortorder, PDO::PARAM_INT );

        $stmt->execute();
    }
}


?>