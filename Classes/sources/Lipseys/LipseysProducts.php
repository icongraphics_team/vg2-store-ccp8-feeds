<?php
namespace VG;

class LipseysProducts extends Products{

    /**
     * getInventory
     * Fetches Lipseys Inventory XML Feed, creates LipseysProduct from data, then updates it in the database with correct inventory/pricing information.
     * @return int                  Number of products updated.
     */
    public function getInventory(){
        $inventory = new LipseysFeed( "inventory" );
        $inventory->downloadFeed();
        $count = 0;
        foreach( $inventory->extractItems() as $item){
            $product = new LipseysProduct( array(
                    'id' => trim( $item['UPC'] ),
                    'inventory_level' => $item['QtyOnHand'],
                    'pricing_type' => ( $item['OnSale'] === "Y" ) ? "sale" : "",
                    'wholesale_price' => $item['Price'],
                    'map_price' => ($item['RetailMAP'] === "0") ? null : $item['RetailMAP']
	            )
            );
            $product->updateEcom();
            $count++;
        }
        return $count;
    }

    /**
     * getProducts
     * Fetches Lipseys Product XML Feed, creates Lipseys Products from data, then adds them to the database.
     * @param string $item_type
     * @return mixed
     */
    public function getProducts( $item_type = "FIREARM" ){
        $catalog = new LipseysFeed( "catalog", $item_type );
        $catalog->downloadFeed();

        foreach( $catalog->extractItems() as $catalog_item ) {
            $upc = trim( $catalog_item['UPC'] );
            $description = ( empty($catalog_item['Desc2']) ? $catalog_item['Desc1'] : $catalog_item['Desc2']);
            $description .= "<ul class='product-description-list'>";
            $description .= (!empty($catalog_item['Model'])) ? "<li>Model: {$catalog_item['Model']}</li>" : "";
            $description .= (!empty($catalog_item['Caliber'])) ? "<li>Caliber / Gauge: {$catalog_item['Caliber']}</li>" : "";
            $description .= (!empty($catalog_item['Type'])) ? "<li>Type: {$catalog_item['Type']}</li>" : "";
            $description .= (!empty($catalog_item['Action'])) ? "<li>Action: {$catalog_item['Action']}</li>" : "";
            $description .= (!empty($catalog_item['Finish'])) ? "<li>Finish: {$catalog_item['Finish']}</li>" : "";
            $description .= (!empty($catalog_item['StockFrame'])) ? "<li>Stock / Frame: {$catalog_item['StockFrame']}</li>" : "";
            $description .= (!empty($catalog_item['Weight'])) ? "<li>Weight: {$catalog_item['Weight']}</li>" : "";
            $description .= (!empty($catalog_item['Length'])) ? "<li>Length: {$catalog_item['Length']}</li>" : "";
            $description .= (!empty($catalog_item['Capacity'])) ? "<li>Capacity: {$catalog_item['Capacity']}</li>" : "";
            $description .= (!empty($catalog_item['Magazine'])) ? "<li>Magazine: {$catalog_item['Magazine']}</li>" : "";
            $description .= (!empty($catalog_item['Sights'])) ? "<li>Sights: {$catalog_item['Sights']}</li>" : "";
            $description .="</ul>";
            $this->products_list[] = new LipseysProduct( array(
                    'id' => $upc,
                    'display_name' => $catalog_item['Desc1'],
                    'product_number' => $catalog_item['ItemNo'],
                    'product_keywords' => "{$catalog_item['ItemNo']}, {$catalog_item['MFGModelNo']}, {$catalog_item['MFG']}, {$catalog_item['Type']}, {$catalog_item['Action']}, {$catalog_item['Caliber']}, {$catalog_item['Model']}",
                    'description_short' => ( empty($catalog_item['Desc1']) ? $upc : $catalog_item['Desc1']),
                    'description_long' => $description,
                    'pricing_type' => ( isset( $inventory[$upc] ) ) ? $inventory[$upc]['OnSale'] : "",
                    'regular_price' => ( isset( $inventory[$upc] ) ) ? $inventory[$upc]['Price'] : "",
                    'sale_price' => ( isset( $inventory[$upc] ) ) ? $inventory[$upc]['Price'] : "",
                    'product_image' => $catalog_item['Image'],
                    'inventory_level' => ( isset( $inventory[$upc] ) ) ? $inventory[$upc]['QtyOnHand'] : "",
                    'manufacturer' => $catalog_item['MFG'],
                    'action' => $catalog_item['Action'],
                    'type' => trim( $catalog_item['Type'] ),
                    'model' => trim( $catalog_item['Model'] ),
                    'item_type' => ( isset( $catalog_item['ItemType'] ) ) ? $catalog_item['ItemType'] : "",
                    'barrel' => ( isset( $catalog_item['Barrel'] ) ) ? $catalog_item['Barrel'] : "",
                    'capacity' => ( isset( $catalog_item['Capacity'] ) ) ? $catalog_item['Capacity'] : "",
                    'finish' => ( isset( $catalog_item['Finish'] ) ) ? $catalog_item['Finish'] : "",
                    'length' => ( isset( $catalog_item['Length'] ) ) ? $catalog_item['Length'] : "",
                    'shipping_weight' => ( isset( $catalog_item['ShippingWeight'] ) ) ? $catalog_item['ShippingWeight'] : "",
                    'feed_type_source' => $item_type,
                    'source_data' => json_encode( array(
                        PRODFILTERONE => ( !empty( $catalog_item[ucfirst(PRODFILTERONE)] ) )? $catalog_item[ucfirst(PRODFILTERONE)] : "",
                        PRODFILTERTWO => ( !empty( $catalog_item[ucfirst(PRODFILTERTWO)] ) )? $catalog_item[ucfirst(PRODFILTERTWO)] : "",
                        PRODFILTERTHREE => ( !empty( $catalog_item[ucfirst(PRODFILTERTHREE)] ) )? $catalog_item[ucfirst(PRODFILTERTHREE)] : "",
                        PRODFILTERFOUR => ( !empty( $catalog_item[ucfirst(PRODFILTERFOUR)] ) )? $catalog_item[ucfirst(PRODFILTERFOUR)] : "",
                        PRODFILTERFIVE => ( !empty( $catalog_item[strtoupper(PRODFILTERFIVE)] ) )? $catalog_item[strtoupper(PRODFILTERFIVE)] : "",
                    )),
                    'msrp_price' => $catalog_item['MSRP'],
                )
            );
        }
        return $this->products_list;
    }

}

?>