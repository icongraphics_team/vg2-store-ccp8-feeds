<?php
namespace VG;
use \PDO;

class LipseysCategories extends Categories {

    public function getEcomCategories(){
        $dbh = DBH::getInstance("ecom");
        $sql = "SELECT xcat FROM ecom_prod";
        $result = $dbh->query( $sql );
        while ( $product = $result->fetch( PDO::FETCH_ASSOC ) ){
            $product_categories = explode( ", ", $product['xcat'] );
            foreach( $product_categories as $category ){
                if( !array_key_exists( $category, $this->categories ) && $category !== "" ){

                    $this->categories[$category] = new LipseysCategory( array(
                            'id' => make_id( $category ),
                            'name' => $category,
                            'description' => "The {$category} category",
                            'catimg' => 'arf.jpg',
                        )
                    );
                    $this->categories[$category]->addToEcom();
                }
            }
        }

        return $this->categories;
    }
}

?>
