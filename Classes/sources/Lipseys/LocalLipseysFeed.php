<?php

namespace VG;

class LocalLipseysFeed extends LocalXMLFeed{

    function __construct( $fresh = true, $type = 'inventory', $remote_feed_obj = null ) {
        if( is_null( $remote_feed_obj ) ) {
            $remote_feed_obj = new RemoteLipseysFeed( $type, DBH::getInstance() );
        }

        if( $fresh !== false && is_subclass_of( $remote_feed_obj, 'RemoteXMLFeed' ) ) {
            $this->output_full_path = $remote_feed_obj->output_full_path;
        } else{
            $this->output_full_path = $this->getLatestFeedPath( $type );
            $this->cleanOldfiles( $type );
        }
    }

    /**
     * Returns array of object of each items found in XML file.
     * @return array list of objects found in XML file.
     */
    public function getItems(){
        if( is_null( $this->output_full_path ) ) {
            die( 'invalid XML path' );
        }

        $xml_feed = simplexml_load_file( $this->output_full_path );
        foreach( $xml_feed->Item as $item ) {
            $this->items[trim( $item->UPC )] = get_object_vars( $item );
            $this->items[trim( $item->UPC )]['time'] = time();
        }

        return $this->items;
    }

    protected function cleanOldFiles( $feed_type ){
        if( $feed_type !== "catalog" && $feed_type !== "inventory" ){
            echo "Invalid Feed Type Provided During Cleaning: " . $feed_type;
        } else{
            $matches = [];
            foreach( glob(XMLOUTPUTPATH . '/*.xml' ) as $filename ) {
                if( preg_match("/Lipseys_" . $feed_type . "_[0-9]*.xml/", $filename ) ){
                    if( $this->output_full_path !== $filename ){
                        unlink( $filename );
                    }
                }
            }
        }
    }
}

?>