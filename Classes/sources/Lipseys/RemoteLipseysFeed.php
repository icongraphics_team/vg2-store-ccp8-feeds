<?php

namespace VG;

class RemoteLipseysFeed extends RemoteXMLFeed {
    protected $name_prefix = "Lipseys_";

    function __construct( $type, \PDO $dbh ){
        $this->dbh = $dbh;

        $this->type = $type;
        $this->setTime();
        $this->setOutputName();
        $this->setOutputFullPath();

        switch ( $this->type ) {
            case 'catalog':
                $this->feed_download_url = "http://www.lipseys.com/API/catalog.ashx?email=sales@valleyguns2.com&pass=model70";
                break;
            
            case 'inventory':
                $this->feed_download_url = "http://www.lipseys.com/API/pricequantitycatalog.ashx?email=sales@valleyguns2.com&pass=model70";
                break;

            default:
                $this->feed_download_url = null;
                break;
        }

        $this->downloadFresh();
    }
}

?>