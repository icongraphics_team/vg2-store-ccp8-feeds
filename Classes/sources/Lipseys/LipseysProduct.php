<?php
namespace VG;
use \PDO;

class LipseysProduct extends Product{

    public $category = [];
	public $map_price = "";
	public $wholesale_price = "";
    public $msrp_price = "";

    /**
     * getImage
     * Downloads product's image to ecom's image directory if image doesn't already exist.
     */
    protected function getImage(){
        $remote_url = "http://www.lipseys.net/images/" . $this->product_image;
        $local_path = PRODUCTIMAGEPATH . "/" . $this->product_image;
        if( ! file_exists( $local_path ) ) {
            $find = array( 'images//',' ') ;
            $replace = array( 'images/', '%20' );
            $remote_file = str_replace($find, $replace, $remote_url );
            $image = file_get_contents( $remote_file );
            file_put_contents( $local_path, $image );
            log_it( 'image_added', 'Added ' . $this->product_image );
        }
    }

    protected function getPrice($category){
	    switch($this->getCategoryOnInventoryPull($category)){
		    case "accessory":
		        $tmp_price = $this->getPriceFromExisting();
		    	$price_final = ( $tmp_price !== false ) ? $tmp_price : ($this->wholesale_price / .85) + 25;
		    	break;
		    case "handgun":
		    	$price_final = ($this->wholesale_price / .85) + 25;
			    break;
		    default:
			    $price_final = ($this->wholesale_price / .85) + 30;
			    break;

		    if($price_final < $this->map_price){
			    $price_final = $this->map_price;
		    }
	    }

	    return round($price_final, 2);
    }

    /**
     * updateEcom
     * Updates product in ecommerce with inventory and price information
     */
    public function updateEcom(){
        try{
			$dbh = DBH::getInstance("ecom");
			$sql = "SELECT xcat FROM ecom_prod WHERE id = :id";
	        $stmt = $dbh->prepare($sql);
	        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
	        $stmt->execute();
	        $ecom_prod = $stmt->fetchObject();

	        if($ecom_prod !== false) {
		        $sql  = "UPDATE ecom_prod SET invlevel = :invlevel, regprice = :regprice WHERE id = :id";
		        $stmt = $dbh->prepare( $sql );
		        $stmt->bindParam( ':invlevel', $this->inventory_level, PDO::PARAM_INT );
		        $stmt->bindValue( ':regprice', $this->getPrice( $ecom_prod->xcat ), PDO::PARAM_STR );
		        $stmt->bindParam( ':id', $this->id, PDO::PARAM_INT );
		        $stmt->execute();
		        log_it( 'inventory', 'Updated ' . $this->id );
	        }
        } catch(\PDOException $e){
            echo "ERROR: " . $e->getMessage();
        }
    }

    /**
     * getCategory
     * Creates new LipseysCategory object, adds it to database, then returns it for use in product creation.
     * @return array
     */
    public function getCategory(){
        $category = new LipseysCategory( make_id( $this->type ) );
        $category->addToEcom();
        $this->category = $category->id;

        return $this->category;
    }

    public function getCategoryOnInventoryPull($category = "Rifle"){
		$accessories_categories = ['ACCESSORY-MAGAZINE-RELEASE','ACCESSORY-ACTIONS','ACCESSORY-AMMUNITION','ACCESSORY-ASSORTMENT','ACCESSORY-BARRELS','ACCESSORY-BINOCULARS','ACCESSORY-BOLTS','ACCESSORY-BRACES','ACCESSORY-CASES','ACCESSORY-ENTRENCHING-TOOL','ACCESSORY-FLASH-HIDERS','ACCESSORY-FORENDS','ACCESSORY-FRAMES','ACCESSORY-GRIPS','ACCESSORY-HOLSTERS','ACCESSORY-KITS','ACCESSORY-KNIVES','ACCESSORY-LASERS-AND-SIGHTS','ACCESSORY-LIGHTS','ACCESSORY-MAGAZINE-POUCHES','ACCESSORY-MAGAZINES','ACCESSORY-MUZZLE-BRAKES','ACCESSORY-RAILS','ACCESSORY-RANGE-BAGS','ACCESSORY-RANGE-FINDERS','ACCESSORY-RECEIVER-SET','ACCESSORY-RIFLE','ACCESSORY-RINGS/MOUNTS/BASES','ACCESSORY-SCOPES','ACCESSORY-SILENCER-ACCESSORIES','ACCESSORY-SLINGS-AND-SWIVELS','ACCESSORY-SPOTTING-SCOPES','ACCESSORY-STOCKS','ACCESSORY-TOOLS','ACCESSORY-TRIGGER','ACCESSORY-UPPERS','NFA-SILENCER'];
		//$longgun_categories = ['COMBO','MUZZLELOADER','NFA-SHORT-BARREL-RIFLE','RIFLE','SHOTGUN'];
		$handgun_categories = ['REVOLVER','SEMI-AUTO-PISTOL','SPECIALTY-HANDGUN'];

		if(in_array($category, $accessories_categories)){
			return "accessory";
		} elseif(in_array($category, $handgun_categories)){
			return "handgun";
		} else{
			return "longgun";
		}
    }

    public function getSeoUrl(){
	    $url = str_replace(' ', '-', $this->display_name);
	    $url = $url . '-' . $this->id;
	    $url = preg_replace('/[^A-Za-z0-9\-]/', '', $url);
	    return $url;
    }

    /**
     * fixName
     * Prepares product name for use.
     * @param string $string            Product name.
     * @return string                   Fixed Product name.
     */
    protected function fixName( $string = "" ){
        $lc = strtolower( $string );
        $fixed = ucwords( $lc );

        return $fixed;
    }

    public function getPriceFromExisting(){
        try{
            $dbh = DBH::getInstance("ecom");
            $sql = "SELECT msrp FROM ecom_prod WHERE id = :id LIMIT 1";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchObject();

            if( $this->wholesale_price < 100 ){
                return $result->msrp;
            } else{
                if( ( $this->map_price > 0 ) && ( $this->map_price < $result->msrp ) ){
                    return $this->map_price;
                } else{
                    return $result->msrp;
                }
            }
        } catch(\PDOException $e){
            return false;
        }
    }

    /**
     * addToEcom
     * Adds LipseysProduct object to database.
     */
    public function addToEcom(){
        try{
            $dbh = DBH::getInstance("ecom");
            $prod_cols = array( 'id', 'name', 'prodnum', 'pricestatus', 'regprice', 'saleprice', 'volprice', 'recurprice', 'whlprice', 'affilcomm', 'keywords', 'descshort', 'desclong', 'descmobile', 'lgdisp', 'prodmulti', 'imgsm', 'imglg', 'imgxl', 'splashdisp', 'newdisp', 'specialsdisp', 'bestselldisp', 'featuredisp1', 'featuredisp2', 'featuredisp3', 'featuredisp4', 'featuredisp5', 'taxstateprov', 'taxcountry', 'useinv', 'invlevel', 'addemtext', 'delmethod', 'customship', 'dlfile', 'shiponename', 'shiponeprice', 'shiptwoname', 'shiptwoprice', 'shipthreename', 'shipthreeprice', 'shiplocal', 'shipint', 'shiplength', 'shipwidth', 'shipheight', 'shipweight', 'shipnumbox', 'shipstateprov', 'shipcountry', 'shipzip', 'shipemail', 'xcat', 'xprod', 'xupsell', 'xprodoptions', 'sortorder', 'sortprice', 'prodidx', 'prodview', 'prodviewg', 'minquan', 'maxquan', 'taxclass', 'datetimestamp', 'metatitle', 'metadesc', 'seourl', 'reviewsavg', 'reviewsnum', 'notifystock', 'xprodfilterone', 'xprodfiltertwo', 'xprodfilterthree', 'xprodfilterfour', 'xprodfilterfive', 'loyaltypoints', 'tabonename', 'tabonecontent', 'tabtwoname', 'tabtwocontent', 'tabthreename', 'tabthreecontent', 'tabfourname', 'tabfourcontent', 'autoadded', 'feed_type_source', 'source_data', 'msrp' );
            $sql_cols = "";
            $sql_vals = "";
            $sql_update = "";
            $total_cols = count( $prod_cols );
            $updatable_vals = array( 'name', 'prodnum', 'keywords', 'descshort', 'desclong', 'descmobile', 'imgsm', 'imglg', 'seourl', 'imgxl', 'xprodfilterone', 'xprodfiltertwo', 'xprodfilterthree', 'xprodfilterfour', 'xprodfilterfive' );
            $last_updatable = array_values( $updatable_vals );
            $last_updatable = end( $last_updatable );
            $i = 0;

            foreach( $prod_cols as $prod_col ){
                $i++;
                $sql_cols .= "{$prod_col}";
                $sql_vals .= ":{$prod_col}";

                if( in_array( $prod_col, $updatable_vals ) ) {
                    $sql_update .= "{$prod_col} = :{$prod_col}";
                    if( $prod_col !== $last_updatable ){
                        $sql_update .= ", ";
                    }
                }

                if( $i < $total_cols ){
                    $sql_cols .= ", ";
                    $sql_vals .= ", ";
                }

            }

            $sql = "INSERT INTO ecom_prod ({$sql_cols}) VALUES({$sql_vals}) ON DUPLICATE KEY UPDATE {$sql_update}";

            $stmt = $dbh->prepare( $sql );
            $source_data = json_decode( $this->source_data, true );
            $stmt->bindParam( ':id', $this->id, PDO::PARAM_STR );
            $stmt->bindValue( ':name', $this->fixName( $this->display_name ), PDO::PARAM_STR );
            $stmt->bindParam( ':prodnum', $this->product_number, PDO::PARAM_STR );
            $stmt->bindValue( ':pricestatus', "R", PDO::PARAM_STR );
            $stmt->bindValue( ':regprice', $this->regular_price, PDO::PARAM_STR );
            $stmt->bindParam( ':saleprice', $this->sale_price, PDO::PARAM_STR );
            $stmt->bindValue( ':volprice', "0.00", PDO::PARAM_STR );
            $stmt->bindValue( ':recurprice', "0.00", PDO::PARAM_STR );
            $stmt->bindValue( ':whlprice', "0.00", PDO::PARAM_STR );
            $stmt->bindValue( ':affilcomm', "0.00", PDO::PARAM_STR );
            $stmt->bindParam( ':keywords', $this->product_keywords, PDO::PARAM_STR );
            $stmt->bindParam( ':descshort', $this->description_short, PDO::PARAM_STR );
            $stmt->bindParam( ':desclong', $this->description_long, PDO::PARAM_STR );
            $stmt->bindParam( ':descmobile', $this->description_short, PDO::PARAM_STR );
            $stmt->bindValue( ':lgdisp', "default", PDO::PARAM_STR );
            $stmt->bindValue( ':prodmulti', 0, PDO::PARAM_INT );
            $stmt->bindParam( ':imgsm', $this->product_image, PDO::PARAM_STR );
            $stmt->bindParam( ':imglg', $this->product_image, PDO::PARAM_STR );
            $stmt->bindParam( ':imgxl', $this->product_image, PDO::PARAM_STR );
            $stmt->bindValue( ':splashdisp', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':newdisp', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':specialsdisp', 0, PDO::PARAM_INT);
            $stmt->bindValue( ':bestselldisp', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':featuredisp1', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':featuredisp2', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':featuredisp3', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':featuredisp4', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':featuredisp5', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':taxstateprov', "STANDARD", PDO::PARAM_STR );
            $stmt->bindValue( ':taxcountry', "STANDARD", PDO::PARAM_STR );
            $stmt->bindValue( ':useinv', 1, PDO::PARAM_INT );
            //TEMP INVENTORY!!!!
            $stmt->bindValue( ':invlevel', 0, PDO::PARAM_INT );
            //END TEMP INVENTORY!!!!
            $stmt->bindValue( ':addemtext', "", PDO::PARAM_STR );
            $stmt->bindValue( ':delmethod', "P", PDO::PARAM_STR );
            $stmt->bindValue( ':customship', "", PDO::PARAM_STR );
            $stmt->bindValue( ':dlfile', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shiponename', "Free Shipping", PDO::PARAM_STR );
            $stmt->bindValue( ':shiponeprice', "0.00", PDO::PARAM_STR );
            $stmt->bindValue( ':shiptwoname', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shiptwoprice', "0.00", PDO::PARAM_STR );
            $stmt->bindValue( ':shipthreename', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shipthreeprice', "0.00", PDO::PARAM_STR );
            $stmt->bindValue( ':shiplocal', "UPSTOOLS", PDO::PARAM_STR );
            $stmt->bindValue( ':shipint', "UPSTOOLS", PDO::PARAM_STR );
            $stmt->bindValue( ':shiplength', "0", PDO::PARAM_STR );
            $stmt->bindValue( ':shipwidth', "0", PDO::PARAM_STR );
            $stmt->bindValue( ':shipheight', "0", PDO::PARAM_STR );
            $stmt->bindValue( ':shipweight', $this->shipping_weight, PDO::PARAM_STR );
            $stmt->bindValue( ':shipnumbox', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shipstateprov', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shipcountry', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shipzip', "", PDO::PARAM_STR );
            $stmt->bindValue( ':shipemail', "", PDO::PARAM_STR );
            $stmt->bindValue( ':xcat', $this->getCategory(), PDO::PARAM_STR );
            $stmt->bindValue( ':xprod', "", PDO::PARAM_STR );
            $stmt->bindValue( ':xupsell', "", PDO::PARAM_STR );
            $stmt->bindValue( ':xprodoptions', "", PDO::PARAM_STR );
            $stmt->bindValue( ':sortorder', 0, PDO::PARAM_STR );
            $stmt->bindValue( ':sortprice', str_replace( '.', '', $this->regular_price ), PDO::PARAM_INT );
            $stmt->bindValue( ':prodidx', "S", PDO::PARAM_STR );
            $stmt->bindValue( ':prodview', "A", PDO::PARAM_STR );
            $stmt->bindValue( ':prodviewg', "", PDO::PARAM_STR );
            $stmt->bindValue( ':minquan', 0, PDO::PARAM_INT);
            $stmt->bindValue( ':maxquan', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':taxclass', "STANDARD", PDO::PARAM_STR );
            $stmt->bindValue( ':datetimestamp', time(), PDO::PARAM_STR );
            $stmt->bindParam( ':metatitle', $this->display_name, PDO::PARAM_STR );
            $stmt->bindParam( ':metadesc', $this->description_short, PDO::PARAM_STR );
            $stmt->bindValue( ':seourl', $this->getSeoUrl(), PDO::PARAM_STR );
            $stmt->bindValue( ':reviewsavg', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':reviewsnum', 0, PDO::PARAM_STR );
            $stmt->bindValue( ':notifystock', "", PDO::PARAM_STR );
            $stmt->bindValue( ':xprodfilterone', make_id( clean_filter_name( $source_data[PRODFILTERONE] ) ), PDO::PARAM_STR );
            $stmt->bindValue( ':xprodfiltertwo', make_id( clean_filter_name( $source_data[PRODFILTERTWO] ) ), PDO::PARAM_STR );
            $stmt->bindValue( ':xprodfilterthree', make_id( clean_filter_name( $source_data[PRODFILTERTHREE] ) ), PDO::PARAM_STR );
            $stmt->bindValue( ':xprodfilterfour', make_id( clean_filter_name( $source_data[PRODFILTERFOUR] ) ), PDO::PARAM_STR );
            $stmt->bindValue( ':xprodfilterfive', make_id( clean_filter_name( $source_data[PRODFILTERFIVE] ) ), PDO::PARAM_STR );
            $stmt->bindValue( ':loyaltypoints', 0, PDO::PARAM_INT );
            $stmt->bindValue( ':tabonename', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabonecontent', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabtwoname', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabtwocontent', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabthreename', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabthreecontent', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabfourname', "", PDO::PARAM_STR );
            $stmt->bindValue( ':tabfourcontent', "", PDO::PARAM_STR );
            $stmt->bindValue( ':autoadded', 1, PDO::PARAM_INT );
            $stmt->bindParam( ':feed_type_source', $this->feed_type_source, PDO::PARAM_STR );
            $stmt->bindParam( ':source_data', $this->source_data, PDO::PARAM_STR );
            $stmt->bindParam( ':msrp', $this->msrp_price, PDO::PARAM_STR);

            $stmt->execute();

            return $stmt->rowCount();

        }catch(\PDOException $e){
            echo "ERROR: " . $e->getMessage();
        }
    }

}

?>