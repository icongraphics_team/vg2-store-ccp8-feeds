<?php
namespace VG;
use PDO;

class LipseysFilters{

    public $filters = [];
    public $filter_count = 0;

    public function getEcomFilters(){
        $dbh = DBH::getInstance("ecom");
        $sql = "SELECT id, source_data FROM ecom_prod";
        $result = $dbh->query( $sql );
        $filter_types = array( PRODFILTERONE, PRODFILTERTWO, PRODFILTERTHREE, PRODFILTERFOUR, PRODFILTERFIVE );
        foreach( $filter_types as $filter_type ){
            $this->filters[$filter_type] = array();
        }

        while ( $data = $result->fetch( PDO::FETCH_ASSOC ) ){
            $source_data = json_decode( $data['source_data'], true );
            foreach( $filter_types as $filter_type ){
                if( !in_array( $source_data[$filter_type], $this->filters[$filter_type] ) && !empty( $source_data[$filter_type] ) ){
                    $name = $source_data[$filter_type];
                    $this->filters[$filter_type][$name] = new LipseysFilter( $name, $filter_type );
                    $this->filters[$filter_type][$name]->addToEcom();
                    $this->filter_count++;
                }
            }
        }

        return $this->filters;
    }

}

?>