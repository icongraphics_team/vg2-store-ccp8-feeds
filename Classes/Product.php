<?php
namespace VG;

Abstract class Product {
    public $id = null;
    public $display_name;
    public $product_number; //UPC
    public $product_keywords;
    public $description_short;
    public $description_long;
    public $pricing_type; //On sale?
    public $regular_price;
    public $sale_price;
    public $product_image;
    public $inventory_level;
    public $manufacturer;
    public $action;
    public $type;
    public $item_type;
    public $barrel;
    public $capacity;
    public $finish;
    public $length;
    public $shipping_weight;
    public $feed_type_source;
    public $prod_filter_1;
    public $prod_filter_2;
    public $prod_filter_3;
    public $prod_filter_4;
    public $prod_filter_5;
    public $source_data;
    public $category = "";

    function __construct( $properties ) {
        foreach( $properties as $key => $value) {
            $this->$key = $value;
        }

        $this->getImage();
    }

    abstract protected function getImage();

    abstract protected function addToEcom();

}


?>