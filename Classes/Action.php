<?php

namespace VG;
use \DateTime;
use \PDO;
use \Imagick;

class Action{
    protected $time_s;
    protected $time_e;
    protected $xoption;
    protected $log;

    public function __construct( $xoption = false ){
        $this->xoption = $xoption;
        $this->log = new Log;
    }

    /**
     * timeAction
     * Displays time taken to perform an action (for cronjob emails and debugging).
     *
     * @param string $point             Starting or stopping an action?.
     * @param null $method              Method used (passed to it via __METHOD__).
     * @param int $total_items          Counter for how many items (images, products, categories, etc.) are processed.
     */
    protected function timeAction( $point = "start", $method = null, $total_items = 0 ){
        if( $point === "start" ){
            $this->time_1 = new DateTime;
        } elseif( $point === "stop" ){
            $this->time_2 = new DateTime;
            $difference = $this->time_1->diff($this->time_2);
            echo "{$method} executed in {$difference->format('%s')} seconds";
        
            if( $this->xoption !== false ){
                echo "\n More Info: {$this->xoption}";
            }

            echo " {$total_items} Total Items ";

            echo "Memoray Usage: " . $this->memUsage();
        }
    }

    /**
     * memUsage
     * Takes memory used to execute an action and converts it to B, KB or MB (Used exclusively as part of the "Stop" timeAction point.
     * @return string                   Memory usage in B, KB or MB.
     */
    protected function memUsage(){
        $usage = memory_get_usage( true );
        if( $usage < 1024 ){
            return $usage . " bytes";
        } elseif( $usage < 1048576 ){
            return round( $usage / 1024, 2 ) . " kilobytes";
        } else{
            return round( $usage / 1048576, 2 ) . " megabytes";
        }
    }

    public function maintenance(){
        $maintenance = new Maintenance;
        $maintenance->clearLogs();
        $this->ecomImageResize( 'sm' );
        $this->ecomImageResize( 'lg' );
    }

    /**
     * ecomProdLoad
     * 1) Clears out existing products (only ones added by Feeds system
     * 2) Iterates through products of specified types in XML file and adds them to ecommerce system
     * 3) Moves all downloaded images to the appropriate directories.
     */
    public function ecomProdLoad(){
        $this->timeAction("start");

        $products = new LipseysProducts;
//        $dbh = DBH::getInstance("ecom");

        //Delete Existing Products
//        $sql = "DELETE FROM ecom_prod WHERE autoadded = :autoadded AND feed_type_source = :feed_type_source";
//        $stmt = $dbh->prepare($sql);
//        $stmt->bindValue(':autoadded', 1, PDO::PARAM_INT);
//        $stmt->bindParam(':feed_type_source', $this->xoption , PDO::PARAM_STR);
//        $stmt->execute();

        $products = $products->getProducts( $this->xoption );

        foreach ( $products as $product ){
            $status = $product->addToEcom();
            if( $status === 2 ){
                $this->log->createEntry( "Updated Existing Product: {$product->display_name} ({$product->id})" );
            } else{
                $this->log->createEntry( "Added New Product: {$product->display_name} ({$product->id})" );
            }
        }

        move_images();

        $this->timeAction("stop", __METHOD__, count( $products ) );

        $this->log->writeEntry();
    }

    /**
     * ecomInvLoad
     * Calls getInventory method in Products class
     */
    public function ecomInvLoad(){
        $this->timeAction("start");
        $products = new LipseysProducts;
        $inventory = $products->getInventory();
        
        $this->timeAction( "stop", __METHOD__, $inventory );
        $this->log->createEntry( "Inventory Updated: {$inventory} Products Updated" );
        $this->log->writeEntry();
    }

    /**
     * ecomImageResize
     * Resizes default, XL images as either SM or LG
     * 1) Iterates through all images in designated folder, resizes if they're oversized.
     * 2) Counts number of images processed and passes it to timeAction method.
     *
     * @param null $xoption                 Image size to process.
     */
    public function ecomImageResize( $xoption = null ){
        $this->timeAction( "start" );
        if( is_null( $xoption ) ){
            $xoption = $this->xoption;
        }
        switch ( $xoption ) {
            case 'sm':
                $path = FINALPRODUCTIMAGEPATHSM . '/*.jpg';
                $max_width = 200; //pixels
                $max_filesize = 50000; //bytes
                $compression_quality = 70; //percent
                break;
            default:
                $path = FINALPRODUCTIMAGEPATHLG . '/*.jpg';
                $max_width = 600; //pixels
                $max_filesize = 500000; //bytes
                $compression_quality = 85; //percent
                break;
        }

        $images =  glob( $path );
        $i = 0;
        foreach( $images as $image_original ){
            if( filesize( $image_original ) > 0 ){
                $image = new Imagick( $image_original );
                if( ( $image->getImageWidth() > $max_width || $image->getImageLength() > $max_filesize ) && $image->getImageLength() > 0 ) {
                    $image->thumbnailImage( $max_width, 0 );
                    $image->setImageCompressionQuality( $compression_quality );
                    $image->writeImage();
                    $i++;
                }
            }
        }

        $this->timeAction( "stop", __METHOD__, $i );
    }

    /**
     * ecomFilterLoad
     * Calls getEcomFilters method in LipseysFilters class and returns count to timeAction method.
     */
    public function ecomFilterLoad(){
        $this->timeAction( "start" );
        $filters = new LipseysFilters();
        $filters->getEcomFilters();
        $this->timeAction( "stop", __METHOD__, $filters->filter_count );
        $this->log->createEntry( "Filters Updated: {$filters->filter_count} Filters Updated" );
        $this->log->writeEntry();
    }

}

?>