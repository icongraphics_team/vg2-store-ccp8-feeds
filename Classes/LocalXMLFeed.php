<?php

namespace VG;

Abstract class LocalXMLFeed{
    public $output_full_path;
    public $items = array();

    function __construct( $fresh = true, $type = 'inventory', $remote_feed_obj = null ) {

        if( $fresh !== false && is_subclass_of( $remote_feed_obj, 'RemoteXMLFeed' ) ) {
            $this->output_full_path = $remote_feed_obj->output_full_path;
        } else{
            $this->output_full_path = $this->getLatestFeedPath( $type );
        }
    }

    /**
     * Queries the DB for the most recent feed that matches the given type.
     * @param  string $type Type of feed
     * @return string       Full file path of most recent feed's XML file.
     */
    protected function getLatestFeedPath( $type ) {
        $dbh = DBH::getInstance();
        $sql = "SELECT * FROM feed_pulls WHERE type = :type ORDER BY time DESC LIMIT 0, 1";
        $stmt = $dbh->prepare( $sql );
        $stmt->bindParam( ':type', $type, \PDO::PARAM_STR );
        $stmt->execute();
        $latest_feed = $stmt->fetchObject();

        return $latest_feed->file_path;
    }

    protected abstract function cleanOldfiles( $feed_type );

}

?>