<?php
namespace VG;
use \PDO;

class DBH {

    /**
     * Creates database connection
     * @return PDO Object
     */
    public static function getInstance($type = "log") {
        if( $type === "log" ){
            $dbh = new PDO( 'mysql:host=localhost;dbname=vgonline_iconproo_vg2_feed', '', '' );
        } elseif( $type === "ecom" ){
            $dbh = new PDO( 'mysql:host=localhost;dbname=vgonline_ccpdb', '', '' );
        }
        
        $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

        return $dbh;
    }

}

?>