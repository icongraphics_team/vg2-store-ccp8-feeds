<?php

namespace VG;

Abstract class RemoteXMLFeed{
    protected $feed_download_url;
    protected $name_prefix = "defaultprefix_";
    protected $output_name = "defaultname";
    public $output_full_path = null;
    protected $time;
    protected $type = "defaulttype";
    protected $items = array();

    protected $dbh;

    function __construct( $feed_download_url, \PDO $dbh ) {

        if( !filter_var( $feed_download_url, FILTER_VALIDATE_URL ) ) {
            die( 'Class "XMLFeed" given invalid feed URL' );
        } else{
            $this->feed_download_url = $feed_download_url;
        }

        $this->dbh = $dbh;

        $this->setTime();
        $this->setOutputName();
        $this->setOutputFullPath();
        $this->downloadFresh();
    }

    /**
     * Sets the full path of the downloaded XML file
     */
    protected function setOutputFullPath(){
        $this->output_full_path = XMLOUTPUTPATH . "/" . $this->output_name;
    }

    /**
     * Sets the time
     */
    protected function setTime() {
        $this->time = time();
    }

    /**
     * Sets the output name for the XML file
     */
    protected function setOutputName(){
        $this->output_name = $this->name_prefix . $this->type . '_' . $this->time . ".xml";  
    }

    /**
     * Downloads fresh copy of XML file and logs it to the database
     */
    protected function downloadFresh(){
        $xml_feed = file_get_contents( $this->feed_download_url );
        file_put_contents( $this->output_full_path, $xml_feed );
        $this->logToDatabase();
    }

    /**
     * Logs Feed info to database
     */
    protected function logToDatabase(){
        
        try{
            $sql = "INSERT INTO feed_pulls (`file_path`, `time`, `type`) VALUES (:file_path, :time, :type)";
            $stmt = $this->dbh->prepare( $sql );
            $stmt->bindParam( ':file_path', $this->output_full_path, \PDO::PARAM_STR );
            $stmt->bindParam( ':time', $this->time, \PDO::PARAM_STR );
            $stmt->bindParam( ':type', $this->type, \PDO::PARAM_STR );
            $stmt->execute();
        } catch( Exception $e ) {
            die( "Failure to log feed download <br> " . $e->getMessage() );
        }

    }

    /**
     * Returns array of object of each items found in XML file.
     * @return array list of objects found in XML file.
     */
    public function getItems(){
        if( is_null( $this->output_full_path ) ) {
            die( 'invalid XML path' );
        }

        $xml_feed = simplexml_load_file( $this->output_full_path );
        //$items_list = $xml_feed->Item;
        foreach( $xml_feed->Item as $item ) {
            $this->items[] = get_object_vars( $item );
        }

        return $this->items;
    }
}

?>