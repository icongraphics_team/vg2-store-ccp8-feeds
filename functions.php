<?php
namespace VG;
use \Imagick;

/**
 * Logs data
 * @param  string $type type of occurance
 * @param  string $data description of what ocurred.
 * @return bool         true if insert worked, false otherwise
 */
function log_it( $type, $data ) {
    try{
        $dbh = DBH::getInstance("log");
        $sql = "INSERT INTO logs (type, data) VALUES(:type, :data)";
        $stmt = $dbh->prepare( $sql );
        $stmt->bindParam( ':type', $type, \PDO::PARAM_STR );
        $stmt->bindParam( ':data', $data, \PDO::PARAM_STR );
        $stmt->execute();
    } catch(\PDOException $e){
        echo $e->getMessage();
    }
}

function move_images(){
    $images = scandir(PRODUCTIMAGEPATH);
    foreach( $images as $image ){
        if( in_array($image, array(".","..",".htaccess"))) continue;
        $image_sm = FINALPRODUCTIMAGEPATHSM . '/' . $image;
        $image_lg = FINALPRODUCTIMAGEPATHLG . '/' . $image;
        $image_xl = FINALPRODUCTIMAGEPATHXL . '/' . $image;


        if( !file_exists($image_sm) ){
            copy(PRODUCTIMAGEPATH . '/' . $image, $image_sm);
        }

        if( !file_exists($image_lg) ){
            copy(PRODUCTIMAGEPATH . '/' . $image, $image_lg);
        }

        if( !file_exists(FINALPRODUCTIMAGEPATHXL . '/' . $image) ){
            copy(PRODUCTIMAGEPATH . '/' . $image, $image_xl );
        }
    }
}

function make_id( $string, $undo = false ){
    $find = array(
        ' ',
        '.',
        ',',
        '$',
        '!',
	    '/',
    );
    $replace = "-";

    if( $undo === true ){
        return ucfirst( str_replace( "-", " ", $string ) );
    } else{
        return strtoupper( str_replace( $find, $replace, $string ) );
    }
}


function clean_filter_name( $name ){
    if( preg_match( '/Accessory-/', $name ) || preg_match( '/Accessory -/', $name ) ){
        $name = str_replace( array( 'Accessory-', 'Accessory -'), array( '', '' ), $name );
    } elseif( preg_match( '/NFA -/', $name ) || preg_match( '/NFA-/', $name ) ) {
        $name = str_replace( array( 'NFA-', 'NFA -' ), array( '', '' ), $name );
    }

    return $name;
}

?>